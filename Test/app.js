const csvToJson = require("csvtojson")
const fs = require("fs");
const users1CSV = './data/users1.csv';
const users2CSV = './data/users2.csv';
const users1JSON = './data/users1.json';
const users2JSON = './data/users2.json';

function readFromCSVToJson(csvFilePath,jsonFile){
    csvToJson({
        delimiter: "||",  
    })
    .fromFile(csvFilePath)
    .then(result =>{
        result.sort();
        result.forEach((element)=>{
            element.person = {
                name: element.first_name,
                surname:element.last_name
            };  
            element.name = null;
            element.amount = parseFloat(element.amount);
            element.name = element.first_name +' '+ element.last_name;
            element.date = element.date.split("/").reverse().join("-");
            element.cc = stringOnlyNumbers(element.cc);
            element.phone = stringOnlyNumbers(element.phone);
            delete(element.email);
            delete(element.first_name);
            delete(element.last_name);
            delete(element.user);
        })
        let data = JSON.stringify(result,null,2);    
        fs.writeFileSync(jsonFile,data);
    })
}


function stringOnlyNumbers(str){
    let newStr = str.replace(/[^+\d]/g, '');
    return newStr;
}

readFromCSVToJson(users1CSV,users1JSON);
readFromCSVToJson(users2CSV,users2JSON);